
#From weighted_measure, no longer used.
    def TiTorFunction(self, m, dim, P = []):
        """
        This method computes the basic Titterington-Torsney function as a polynomial. This involves getting
        the inverse of the Gram matrix, and then performing a bijective form computation with the padd and pmul,
        or just scalar multiplication as needed.

        Note! P must be a numpy.array. For now, we will cast, but this should be temporary.

        I believe the right interpretation is that our functions are vectors in an inner product space. Possibly
        we could imbue the polynomial arrays with more object structure, which might make *this* function applicable
        to almost polynomials, functions, anything that also had these properties.
        """

        print("TitorFunction is being used")
        print("Currently not used, but may be of use in other codes.")

        #Make P if needed
        if(P == []):
            P = polymod.monoBasis(m)

        #Check for discrepancy
        if(len(P) != m):
            print("Issue in TiTorFunction: m not equal to length of basis. Setting m.")

        #Set up
        P = np.array(P)
        m = len(P)
        M = self.Gram(P)
        Mi = npl.inv(M)

        #Goal: P^T Mi P
        #Right side multiplication: Mi P = Q, column vector.
        Q = [[0.0] for i in range(m)]
        for i in range(m):
            #Q[i] is Mi[i][.] * P[.] #TODO Can you remove this initialization? Just start Q[i] = [0.0]
            #Q[i] = Mi(i, 0)*P[0]
            for j in range(0,m):
                #print("Mi[", i, j, "] is", Mi[i,j], "and P[", j, "] is", P[j], "and their product is", Mi[i,j]*P[i])
                Q[i] = padd(Q[i], Mi[i, j]*P[j])
            #print("Just computed Q[", i, "=", Q[i])

        #Left side multiplication: P^T Q = P . Q
        B = [0.0]
        for i in range(m):
            B = padd(B, pmul(P[i], Q[i]))
            #print("B is now", B)

        #Normalize: #TODO this part might have fucked it up.
        for i in range(len(B)):
            B[i] = B[i]/m

        return B

