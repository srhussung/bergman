#Evaluate Chebyshev_n(x) using the recursive relations
#
#
#
def Chebyshev(n,x, arrayOut = False):

    if (n == 0):
        if(arrayOut):
            return [1.0]
        return 1.0
    elif (n == 1):
        if(arrayOut):
            return [x]
        return x
    elif (n > 1):
        T = [1.0, x]
        for i in range(2,n+1):
            T.append(2*x*T[i-1] - T[i-2])

        if(arrayOut):
            return T
        return T[n]
    else:
        print("Error! Chebyshev evaluated for negative n")
        return 0.0

def nextChebyshev(x,Tim1, Tim2):
    """
    This one just returns the *next* chebyshev.
    Tim1 is T[i-1]
    Tim2 is T[i-2]
    """
    return 2*x*Tim1 - Tim2

"""
#Testing Block: Should make automatic
xDivs = 30
xDivs //= 2
for n in range(1, 5):
    for k in range(-xDivs,xDivs+1):
        x = k/xDivs
        print (x,Chebyshev(n,x))
    print("")
"""
