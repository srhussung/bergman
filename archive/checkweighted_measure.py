import numpy as np

import weighted_measure as wm
import polynomial as polymod

n = 500
outFileNum = 100
BergmanDegree = 12

points  = [-1.0 + 2.0/(n-1)*i for i in range(n)]
weights = [1.0/len(points) for i in points]

mu = wm.weighted_measure(weights, points)

#These are the first few Chebyshev polynomials, remember that we go from high order to low
#So:    x^4  x^3   x^2   x^1    1

p = [[] for i in range(5)]
##Chebyshev
#p[0] = [0.0, 0.0,  0.0,  0.0,  1.0]
#p[1] = [0.0, 0.0,  0.0,  1.0,  0.0]
#p[2] = [0.0, 0.0,  2.0,  0.0, -1.0]
#p[3] = [0.0, 4.0,  0.0, -3.0,  0.0]
#p[4] = [8.0, 0.0, -8.0,  0.0,  1.0]

#Monomials
p = [[1.0 if (i==BergmanDegree-j-1) else 0.0 for i in range(BergmanDegree)] for j in range(BergmanDegree)]

##Check polynomial matrix
#for i in range(12):
#    for j in range(12):
#        print(p[i][j], end=" ")
#    print("")

#Convert to numpy arrays
p = [np.array(q) for q in p]

#print("Norms")
#for i in range(5):
#    print(mu.norm(p[i]))


#for i in range(5):
#    for j in range(i,5):
#        print(i, "and", j, ":", mu.innerProd(p[i], p[j]))
#        #print(mu.innerProd(p[i], p[j]))
#
## Conclusion: These are *not* orthogonal with respect to the Lebesgue measure

##Gram Schmidt Procedure
mu.GramSchmidt(p)

##Check: Check!
#for i in range(5):
#    for j in range(i,5):
#        print(i, j, ":", mu.innerProd(p[i], p[j]))

##Checked!
#polymod.printPoly(mu.BergmanKernel(15), -1, 1, 100)


mu.toString()

outfile = open("output.dat", "w")

for i in range(outFileNum):
    #outfile = open("".join([str(i),".dat"]), "w")
    outfile.write("".join(["Bergman-Kernel-", str(i),"\n"]))
    outfile.write(mu.toString())
    outfile.write("\n")
    #outfile.close()
    mu = mu.PiazzonTransform(n = BergmanDegree)
    print(i, "is done!")

i = outFileNum 
#outfile = open("".join([str(i),".dat"]), "w")
outfile.write("".join(["Bergman-Kernel-", str(i),"\n"]))
outfile.write(mu.toString())
outfile.close()

