#Numerics
import math
import cmath as cm
import numpy as np
from numpy import polyadd as padd
from numpy import polymul as pmul
from numpy import polyval as pval
import numpy.linalg as npl
import numpy.polynomial.polynomial as npp

#Iteration
from itertools import chain

#Custom
import polynomial as polymod

class weighted_measure:
    """
    Description?

    Possible issues.
    We allow for complex points, but not complex weights. This could become an issue.
    """

    weights = []
    points  = []

    #Keeps track of indices after which a line break should be added in printing.
    #I.e., if point_jumps contains i, then points[i] and points[i+1] are not nearby.
    point_jumps = []

    #Will be set in __init__ method
    isComplex = -1 
    dim = -1
    cutoff = 0.0

    def __init__(self, weights, points, point_jumps = [-1], cutoff = 0.0):
        if(len(weights) != len(points)):
            print("Length of arrays doesn't matched in weighted measure initialization.")
            exit()

        #Upload and numpy-ize
        self.weights = np.array(weights)
        self.points  = np.array(points)
        self.point_jumps = point_jumps

        #Cull points with weights below cutoff
        #Apply cutoff, record weight loss and number removed
        toDelete = []
        if(cutoff > 0.0):
            for i, weight in enumerate(self.weights):
                if (weight < cutoff):
                    toDelete.append(i)

        #Remove corresponding
        if(len(toDelete) > 0):
            self.points  = np.delete(self.points, toDelete)
            self.weights = np.delete(self.weights, toDelete)

        #Normalize self.
        self.weights *= 1/self.mass()

        #Check for any complex points in the domain.
        toUnwind = hasattr(points[0], "__iter__")
        if(toUnwind):
            unpacked_points = list(chain.from_iterable(points))
        else:
            unpacked_points = points

        self.isComplex = any([isinstance(x, complex) for x in unpacked_points])
        self.dim = len(points[0]) if toUnwind else 1
        self.cutoff = cutoff

    def __add__(self, nu):
        """
        Overload + operator to add self and another weighted_measure nu
        """

        #Need to:
        #Take union of self.points, nu.points
        #Add weights when both have them, or just take the weight of one.
        #For each point, need to find this point in the other set (if it exists)
        new_points = []
        new_weights = []
        for point, weight in zip(self.points, self.weights):
            #If in self.points
            new_points.append(point)
            new_weights.append(weight)
            if point in nu.points: #If also in nu.points
                i = np.where((nu.points) == point)
                new_weights[-1] += nu.weights[i]

        for point, weight in zip(nu.points, nu.weights):
            if point not in self.points: #If only in nu
                new_points.append(point)
                new_weights.append(weight)

        xi = weighted_measure(new_weights, new_points)

        return xi

    def __mul__(self, alpha):
        """
        Multiply this measure by alpha. (No need to check complexity...?)

        whatever alpha is, it should be compatible with the weights, OR
        have an evaluation function.
        """

        eval_op = getattr(alpha, "eval", None)
        if callable(eval_op):
            #Arguments are function, cutoff
            return self.eval_mul(alpha, cutoff=self.cutoff)
        else:
            new_weights = self.weights*alpha
            return weighted_measure(new_weights, self.points, cutoff=self.cutoff)

    def __rmul__(self, alpha):
        return self*alpha

    def __truediv__(self, alpha):
        """
        Divides the weights by alpha. Alpha better by scalar! (and nonzero)
        """
        return (1.0/alpha)*self

    def __sub__(self, nu):
        return self + (-1)*nu


    def integrateEval(self, p):
        """
        Here p must be a polynomial evaluation function, evaluatable at all points in the 
        weighted measure.
        """
        
        total = 0.0
        for point, weight in zip(self.points, self.weights):
            total += p(point)*weight

        return total
        
    def integrate(self, p):
        """
        Here p must be a polynomial coefficient array, evaluatable at all points in the 
        weighted measure.
        """
       
        #Powers are in high to low order. Switch.
        p.reverse()

        totals = [0.0 for i in self.points]
        for k, poly in enumerate(p):
            totals += (poly*(self.points)**k)*self.weights
        total = sum(totals)

        #Now powers are high to low again
        p.reverse()
        return total

    def mass(self):
        return sum(self.weights)

    def innerProd(self, p, q):
        """
        Here p and q must be polynomial coefficient arrays, 
        evaluatable at all points in the weighted measure.

        Here we are evaluating \int_{\mu} p(z)q(z) d\mu. 
        """

        total = 0.0
        if(not self.isComplex and self.dim == 1):
            #Construct product (Does this work in 2D? Likely no.)
            pq = np.polymul(p,q)  #TODO Here's the problem I think

            ##TODO Test again before removing
            #totalb = sum(np.polyval(pq, self.points)*self.weights)

            evaluated = polymod.eval(pq, self.points)*self.weights
            total = sum(evaluated)
        elif(self.isComplex): #isComplex
            evaluated = polymod.eval(p, self.points)*          \
                        np.conj(polymod.eval(q, self.points))* \
                        self.weights
            total = sum(evaluated)
        elif(self.dim == 2):
            evaluated = polymod.eval(p, self.points)*          \
                        polymod.eval(q, self.points)*          \
                        self.weights
            total = sum(evaluated)

        else:
            print("Don't know how to do this inner product")
            total = 0.0
            

        return total

    def norm(self, p):
        """
        p must be a coefficient array.
        """
        pp = self.innerProd(p,p)
        if(pp < 0.0):
            print("Uh oh, L2 norm of p was negative:", pp, "Setting to 0")
            pp = 0.0
        return math.sqrt(abs(pp))

    def GramSchmidt(self, P, dim = 1):
        """
        P must be an array of coefficient arrays.
        """
        n = len(P)
        #This method is a manual Gram Schmidt decomposition
        #Subtract projections
        for i in range(n):
            for j in range(i+1,n):
                #We will subtract the components of i from each polynomial proceeding
                #Morally: p[j] = p[j] - innerProd(p[i], p[j])*p[i])
                ip_PiPi = self.innerProd(P[i], P[i])
                #print("PiPi", ip_PiPi)
                ip_PiPj = self.innerProd(P[i], P[j])
                #print("PiPj", ip_PiPj)
                #print("PiPj/ip_PiPi*P[i]", ip_PiPj/ip_PiPi*P[i])
                #print("Before subtraction", P[j])

                if dim == 1:
                    P[j] = np.polysub(P[j], ip_PiPj/ip_PiPi*P[i])
                elif dim == 2:
                    P[j] = polymod.polysub2d(P[j], ip_PiPj/ip_PiPi*P[i])
                else:
                    print("No higher d subtraction: weighted_measure.py")
                #print("After subtraction", P[j])
        #Renormalize
        for i in range(n):
            normi = self.norm(P[i])
            if(normi != 0.0):
                P[i] = (1.0/normi)*P[i]
            else:
                P[i] = 0.0*P[i]
#        else:
#            #This method will use numpy and scipy to try and get some speedups.
#
#            #Construct Gram Matrix (Try to be efficient)
#            G = np.matrix([[0.0 for j in range(n)] for i in range()])
#            #Upper half
#            for i in range(n):
#                for j in range(i,n):
#                    G[i, j] = self.innerProd(P[i], P[j])
#
#            #Lower half (Symmetric)
#            for i in range(n):
#                for j in range(i):
#                    G[i, j] = G[j, i]
#
#            #Compute QR, then Rinv
#            (Q, R) = npl.QR(G)
#            Rinv = np.inv(R)
#
#            #Apply Rinv to the polynomial basis--don't change as you go
#
#
#            #Make sure to return the right basis

    def Gram(self, n):
        """
        This method generates a Gram matrix, numpy.matrix style.

        This is a weighted Gram matrix, in that we use self.innerProd to calculate that inner products.

        n is the degree required.
        """
        P = polymod.monoBasis(n, self.dim)
        M = np.matrix([[self.innerProd(P[i], P[j]) for j in range(n)] for i in range(n)])
        return M

    def eval_mul(self, function, cutoff = cutoff):
        """
        This will give us another weighted measure from multiplying by the function.
        I.e., integrating against the function. All we need is the function to have a 
        .eval method. (Someday could we overload the raw parens?)

        n seems to be used for normalization, but this should be obtained from the 
        bergman kernel
        """

        points = self.points 
        weights = self.weights

        evaluated = function.eval(points)
        weights = [(weights[i])*evaluated[i] for i in range(len(points))]


        nu = weighted_measure(weights, points, cutoff = cutoff)

        return nu

    def print(self):
        if(self.isComplex):
            for i in range(len(self.points)):
                print(self.points[i].real, self.points[i].imag, self.weights[i])
            print()
        else: 
            for i in range(len(self.points)):
                print(self.points[i], self.weights[i])
            print()

    def minMax(self, P):
        """
        Returns an array containing the minimum and maximum value of the 
        polynomial over the points in this weighted measure.
        """
        vals = abs(polymod.eval(P, self.points))

        return [min(vals), max(vals)]

    def toString(self):
        """
        Like print, but returns a string so we can direct output manually.
        """
        outStr = ""
        if(self.isComplex):
            for i in range(len(self.points)):
                outStr += " ".join([str(s) for s in [self.points[i].real, self.points[i].imag, self.weights[i], "\n"]])
                if(i in self.point_jumps):
                    outStr += "\n"
            outStr += "\n"

        else: 
            for i in range(len(self.points)):
                if(self.dim == 1):
                    outStr += " ".join([str(s) for s in [self.points[i], self.weights[i], "\n"]])
                else:
                    outStr += " ".join([str(s) for s in [" ".join([str(t) for t in self.points[i]]), self.weights[i], "\n"]])
                if(i in self.point_jumps):
                    outStr += "\n"
            outStr += "\n"
        return outStr

    def __str__(self):
        return self.toString()

    def polyToString(self, P):
        """
        Outputs P evaluated at all points of self as a string.

        For complex, prints both abs and phase. (But phase graphs are weird lol)
        """
        outStr = ""
        if(self.isComplex):
            evaluateA = abs(polymod.eval(P, self.points))
            evaluateP = [cm.phase(z) for z in polymod.eval(P, self.points)]

            for i in range(len(self.points)):
                outStr += " ".join([str(s) for s in [self.points[i].real, self.points[i].imag, evaluateA[i], evaluateP[i], "\n"]])
                if(i in self.point_jumps):
                    outStr += "\n"
            outStr += "\n\n"

        else: 
            evaluate = polymod.eval(P, self.points)
            for i in range(len(self.points)):
                outStr += " ".join([str(s) for s in [self.points[i], evaluate[i], "\n"]])
                if(i in self.point_jumps):
                    outStr += "\n"
            outStr += "\n"
        return outStr

    def evalToString(self, E):
        """
        The function E must have a multipoint evaluation method, E.eval(points)
        """
        outStr = ""
        if(self.isComplex):
            evaluate = E.eval(self.points)
            evaluateA = abs(evaluate)
            evaluateP = [cm.phase(z) for z in evaluate]

            for i in range(len(self.points)):
                outStr += " ".join([str(s) for s in [self.points[i].real, self.points[i].imag, evaluateA[i], evaluateP[i], "\n"]])
                if(i in self.point_jumps):
                    outStr += "\n"
            outStr += "\n\n"

        else: 
            evaluate = E.eval(self.points)
            for i in range(len(self.points)):
                outStr += " ".join([str(s) for s in [self.points[i], evaluate[i], "\n"]])
                if(i in self.point_jumps):
                    outStr += "\n"
            outStr += "\n"
        return outStr

def default(dim = 1, isComplex = False):
    """
    Incase you just need SOME weighted measure
    """
    n = 500
    point_jumps = []

    if(not isComplex):
        if(dim == 1):
            points  = [-1.0 + 2.0/(n-1)*i for i in range(n)]
        if(dim >= 2):
            m = int(math.sqrt(n))
            X = [-1 + (2.0)*i/(m-1)   for _ in range(m) for i in range(m)]
            Y = [-1 + (2.0)*i/(m-1)   for i in range(m) for _ in range(m)]
            points = list(zip(X,Y))
    else: #isComplex
        if(dim == 1):
            radii = 10
            perradii = n//radii

            points = [0.0+0.0j]
            for i in range(1,radii):
                point_jumps += [len(points) - 1]
                r = (i/(radii-1))**0.5
                z = cm.exp(0.0+2*math.pi/(perradii-1)*1.0j)
                for k in range(perradii):
                    points += [r*(z**k)]
        if(dim >= 2):
            print("Not coded yet: weighted_measure.default")

    #Even weights
    weights = [1.0/len(points) for i in points]

    return weighted_measure(weights, points, point_jumps)

if __name__ == "__main__":
    default()

    ##Test block 1
    #mu = default(isComplex=True)
    #for i in range(len(mu.points)):
    #    print(mu.points[i].real, mu.points[i].imag)
    #    if(i in mu.point_jumps):
    #        print("")
    #        print("")

    #Test block 2
    mu = weighted_measure([0.3, 0.3, 0.3], [0, 1, 2])
    nu = weighted_measure([0.1, 0.1, 0.1], [1, 2, 3])

    print("mu")
    print(mu)

    print("nu")
    print(nu)

    print("Sum")
    print(mu+nu)

    print("mu/1.1")
    print(mu/1.1)


    print("mu*2.5j")
    print(2.5j*mu)



