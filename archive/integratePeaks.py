tol = 1e-5

f = open("last_output.dat", "r")
file = f.readlines()
f.close()
A = [[float(a) for a in file[i].split()[0:2]] for i in range(1,len(file))][:-1]

peaks = []
onPeak = False
peakSum = 0.0


for i in range(len(A)):
    #print(A[i])
    if(onPeak):
        #print("on Peak at i =", i, "A[i][1] =", A[i][1])
        peakSum += A[i][1]
        if(A[i][1] < tol):
            #Peak ended:
            onPeak = False
            peakEnd = A[i][0]
            peaks.append([peakStart, peakEnd, peakSum])
            #print("Finishing peak: ", [peakStart, peakEnd, peakSum])
            peakSum = 0.0
    else: #Not on peak
        #print("off Peak at i =", i)
        #Check for peak
        if(A[i][1] > tol):
            #New peak detected:
            onPeak = True
            #print("Starting peak")
            peakStart = A[i][0]
            peakSum = A[i][1]

if(onPeak):
    #print("on Peak at i =", i, "A[i][1] =", A[i][1])
    peakSum += A[i][1]
    #Peak ended:
    onPeak = False
    peakEnd = A[i][0]
    peaks.append([peakStart, peakEnd, peakSum])
    #print("Finishing peak: ", [peakStart, peakEnd, peakSum])
    peakSum = 0.0

for peak in peaks:
    print(peak)
