#Imports
import numpy as np

#Mine
import polynomial as polymod
import weighted_measure_parent as wm
import weighted_measure_complex_1 as wmc1

import bergman

class bergman_kernel_complex_1(bergman.bergman_kernel):
    """
    This is a Bergman class file that will hold information needed to evaluate the Bergman Kernel. 

    This in particular is the COMPLEX SINGLE DIMENSIONAL bergman class file

    This will be the polynomial basis, (after gram-schmidt? before?)

    The Gram matrix relative to this basis, and perhaps an inverse (although... ew. How stable is this?)

    Depending on whether or not the domain is complex, the Bergman Kernel initializer may construct an honest polynomial, which will be stored and used for computation.
    Otherwise, when points need to be evaluated, this will have to be done via the x^T G^{-1} x, where x is the vector of polynomials, each evaluated at this point.
    (Can the whole computation be done through mass computation and matmuls? I don't think so. One to many dimensions maybe, but I can't grok it. 
        This is a lot of computation though. Eesh.


    The idea is that this will have an evaluate method accepting a numpy array of points, and it will return an array with this bergman kernel evaluated at each of these points

    perhaps np.asarray, paired with if(A.shape == ()) to detect scalars.

    """

    #Big settings (should not change within run)
    isComplex = False
    memoizeEvals = False

    originalBasis = []
    orthonormalBasis = []
    mu = []
    dim = 1
    degree = 0

    #For memoization
    oldEvals = {} 

    
    def __init__(self, mu, n, inbasis = []):
        """
        Currently a copy of weighted measure code

        This will generate the BergmanKernel of degree n. 

        "Note: Cannot return with |p| intact, so the complex case 
        would require some real work. Not clear how to do this."

        mu - a weighted measure, carries points and weights
        
        n  - dimension of polynomial space
        """

        self.degree = n

        if(inbasis==[]):
            #Generate basis. [[0 0 1], [0 1 0], [1 0 0]]
            B = polymod.monoBasis(n, self.dim)
        else:
            #TODO Add checks for n matching the basis, square basis, etc
            B = inbasis

        self.originalBasis = B

        #Orthonormalize
        mu.GramSchmidt(B)

        self.orthonormalBasis = B

        #Morally: The Bergman Kernel is the sum of the squares of the norms 
        #of each p in the basis: sum |p|^2

        #Store self variables
        self.isComplex = mu.isComplex #Better be complex!
        if(not self.isComplex):
            print("You have called the complex Bergman for a real measure.")
            exit()

        self.mu = mu
        self.oldEvals = {}

    def eval(self, points):
        """
        Since this is a complex bergman, we cannot just use polynomial evaluation.

        We need to compute
        B(z) = sum_{i=1}^degree B_i(z) * \conjugate{B_i(z)}
        """
        pointArray = np.asarray(points)

        #Deal with single point case
        if pointArray.shape == ():
            pointArray = [complex(pointArray)]

        B = self.orthonormalBasis

        #Evaluate: sum of the squares of the absolute values of each poly at each point.
        ypoints = np.sum([abs(np.polyval(poly, pointArray))**2 for poly in B], axis=0)

        #Normalize
        ypoints /= self.degree

        #Memoization code for real single variable could be used--this is 
        # oldEvals

        #Extract scalar
        if len(ypoints) == 1:
            ypoints = ypoints[0]

        return ypoints

def default(mu = wmc1.default()):
    """
    If you just need SOME Bergman function
    """
    n = 4
    B = bergman_kernel_complex_1(mu, n)

    return B
    

if __name__ == "__main__":
    mu = wmc1.default()
    B = default(mu)
    ypoints = B.eval(mu.points)

    for i in range(len(mu.points)):
        print(mu.points[i], ypoints[i].real, ypoints[i].imag)

    print()
    print("Testing individual point")
    z = 0.4
    print(z, B(z))
    z = -0.2
    print(z, B(z))
    z = 0.4j
    print(z, B(z))
    z = 0.2+0.3j
    print(z, B(z))


