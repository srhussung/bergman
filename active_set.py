from scipy.optimize import nnls
import numpy as np

A = np.array([[1, 0, 0],
              [2, 1, 0.5],
              [0, -1, 1]])

b = np.array([1, 4, 1])

print(nnls(A, b))
print("solves")

print(A)

print(b)
