#Numerics
import math
import cmath as cm
import numpy as np
from numpy import polyadd as padd
from numpy import polymul as pmul
from numpy import polyval as pval
import numpy.linalg as npl
import numpy.polynomial.polynomial as npp

#Iteration
from itertools import chain

#Custom
import polynomial as polymod

class weighted_measure:
    """
    Description?

    Possible issues.
    We allow for complex points, but not complex weights. This could become an issue.
    """

    def __init__(self, weights, points, point_jumps = [-1], cutoff = 0.0):
        if(len(weights) != len(points)):
            print("Length of arrays doesn't matched in weighted measure initialization.")
            exit()

        #Upload and numpy-ize
        self.weights = np.array(weights)
        self.points  = np.array(points)
        self.point_jumps = point_jumps

        #Cull points with weights below cutoff
        #Apply cutoff, record weight loss and number removed
        toDelete = []
        if(cutoff > 0.0):
            for i, weight in enumerate(self.weights):
                if (weight < cutoff):
                    toDelete.append(i)

        #Remove corresponding, hold mass constant
        if(len(toDelete) > 0):
            mass = self.mass()
            self.points  = np.delete(self.points, toDelete)
            self.weights = np.delete(self.weights, toDelete)

            self.normalize(mass)

        #Check for any complex points in the domain.
        toUnwind = hasattr(points[0], "__iter__")
        if(toUnwind):
            unpacked_points = list(chain.from_iterable(points))
        else:
            unpacked_points = points

        self.isComplex = any([isinstance(x, complex) for x in unpacked_points])
        self.dim = len(points[0]) if toUnwind else 1
        self.cutoff = cutoff

    #----------------------------------------------#
    #                                              #
    #                  OPERATORS                   #
    #                                              #
    #----------------------------------------------#

    def __add__(self, nu):
        """
        Overload + operator to add self and another weighted_measure nu
        """

        #Need to:
        #Take union of self.points, nu.points
        #Add weights when both have them, or just take the weight of one.
        #For each point, need to find this point in the other set (if it exists)
        new_points = []
        new_weights = []
        for point, weight in zip(self.points, self.weights):
            #If in self.points
            new_points.append(point)
            new_weights.append(weight)
            if point in nu.points: #If also in nu.points
                i = np.where((nu.points) == point)
                #print("new_weights = ", new_weights)
                #print("nu.weights  = ", nu.weights)
                #print("new_weights[-1] = ", new_weights[-1])
                #print("i = ", i)
                #print("nu.weights[i]   = ", nu.weights[i])
                new_weights[-1] += float(nu.weights[i])

        for point, weight in zip(nu.points, nu.weights):
            if point not in self.points: #If only in nu
                new_points.append(point)
                new_weights.append(weight)

        xi = type(self)(new_weights, new_points)

        return xi

    def __mul__(self, alpha):
        """
        Multiply this measure by alpha. (No need to check complexity...?)

        whatever alpha is, it should be compatible with the weights, OR
        have an evaluation function.
        """

        eval_op = getattr(alpha, "eval", None)
        if callable(eval_op):
            #Arguments are function, cutoff
            return self.eval_mul(alpha)
        else:
            new_weights = self.weights*alpha
            return type(self)(new_weights, self.points, cutoff=self.cutoff)

    def __rmul__(self, alpha):
        return self*alpha

    def eval_mul(self, function):
        """
        This will give us another weighted measure from multiplying by the function.
        I.e., integrating against the function. All we need is the function to have a 
        .eval method. (Someday could we overload the raw parens?)

        n seems to be used for normalization, but this should be obtained from the 
        bergman kernel
        """

        points = self.points 
        weights = self.weights

        evaluated = function.eval(points)
        weights = [(weights[i])*evaluated[i] for i in range(len(points))]


        nu = type(self)(weights, points, cutoff = self.cutoff)

        return nu


    def __truediv__(self, alpha):
        """
        Divides the weights by alpha. Alpha better be scalar! (and nonzero)
        """
        return (1.0/alpha)*self

    def __sub__(self, nu):
        return self + (-1)*nu

    #This for inheritance
    def toString(self):
        return ""
    
    def __str__(self):
        return self.toString()

    def normalize(self, mass = "flag"):
        """
        Normalize this measure to have given mass
        """
        if mass == "flag":
            mass = self.mass()
        self.weights *= 1/mass

    def integrateEval(self, p):
        """
        Here p must be a polynomial evaluation function, evaluatable at all points in the 
        weighted measure.
        """
        
        total = 0.0
        for point, weight in zip(self.points, self.weights):
            total += p(point)*weight

        return total
        
    def integrate(self, p):
        """
        Here p must be a polynomial coefficient array, evaluatable at all points in the 
        weighted measure.
        """
       
        #Powers are in high to low order. Switch.
        p.reverse()

        totals = [0.0 for i in self.points]
        for k, poly in enumerate(p):
            totals += (poly*(self.points)**k)*self.weights
        total = sum(totals)

        #Now powers are high to low again
        p.reverse()
        return total

    def mass(self):
        return sum(self.weights)

    def norm(self, p):
        """
        p must be a coefficient array.
        """
        pp = self.innerProd(p,p)
        if(pp < 0.0):
            print("Uh oh, L2 norm of p was negative:", pp, "Setting to 0")
            pp = 0.0
        return math.sqrt(abs(pp))

    def Gram(self, n):
        """
        This method generates a Gram matrix, numpy.matrix style.

        This is a weighted Gram matrix, in that we use self.innerProd to calculate that inner products.

        n is the degree required.
        """
        P = polymod.monoBasis(n, self.dim)
        M = np.matrix([[self.innerProd(P[i], P[j]) for j in range(n)] for i in range(n)])
        return M

    def minMax(self, P):
        """
        Returns an array containing the minimum and maximum value of the 
        polynomial over the points in this weighted measure.
        """
        vals = abs(polymod.eval(P, self.points))

        return [min(vals), max(vals)]

    def polyToString(self, P):
        """
        Outputs P evaluated at all points of self as a string.

        For complex, prints both abs and phase. (But phase graphs are weird lol)
        """
        outStr = ""
        if(self.isComplex):
            evaluateA = abs(polymod.eval(P, self.points))
            evaluateP = [cm.phase(z) for z in polymod.eval(P, self.points)]

            for i in range(len(self.points)):
                outStr += " ".join([str(s) for s in [self.points[i].real, self.points[i].imag, evaluateA[i], evaluateP[i], "\n"]])
                if(i in self.point_jumps):
                    outStr += "\n"
            outStr += "\n\n"

        else: 
            evaluate = polymod.eval(P, self.points)
            for i in range(len(self.points)):
                outStr += " ".join([str(s) for s in [self.points[i], evaluate[i], "\n"]])
                if(i in self.point_jumps):
                    outStr += "\n"
            outStr += "\n"
        return outStr

#if __name__ == "__main__":
#    default()
#
#    ##Test block 1
#    #mu = default(isComplex=True)
#    #for i in range(len(mu.points)):
#    #    print(mu.points[i].real, mu.points[i].imag)
#    #    if(i in mu.point_jumps):
#    #        print("")
#    #        print("")
#
#    #Test block 2
#    mu = weighted_measure([0.3, 0.3, 0.3], [0, 1, 2])
#    nu = weighted_measure([0.1, 0.1, 0.1], [1, 2, 3])
#
#    print("mu")
#    print(mu)
#
#    print("nu")
#    print(nu)
#
#    print("Sum")
#    print(mu+nu)
#
#    print("mu/1.1")
#    print(mu/1.1)
#
#
#    print("mu*2.5j")
#    print(2.5j*mu)



