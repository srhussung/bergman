#Numerics
import math
import cmath as cm
import numpy as np
from numpy import polyadd as padd
from numpy import polymul as pmul
from numpy import polyval as pval
import numpy.linalg as npl
import numpy.polynomial.polynomial as npp

#Iteration
from itertools import chain

#Custom
import polynomial as polymod
import weighted_measure_parent 

class weighted_measure_complex_1(weighted_measure_parent.weighted_measure):
    """
    This will handle the COMPLEX, 1 DIMENSIONAL weighted measure code.

    First, we have to separate the methods into those that have custom methods and those that don't.
    """

    def innerProd(self, p, q):
        """
        Here p and q must be polynomial coefficient arrays, 
        evaluatable at all points in the weighted measure.

        Here we are evaluating \int_{\mu} p(z)q(z) d\mu. 
        """

        total = 0.0

        evaluated = polymod.eval(p, self.points)*          \
                    np.conj(polymod.eval(q, self.points))* \
                    self.weights

        total = sum(evaluated)
        return total

    def GramSchmidt(self, P, dim = 1):
        """
        P must be an array of coefficient arrays.
        """
        n = len(P)
        #This method is a manual Gram Schmidt decomposition
        #Subtract projections
        for i in range(n):
            for j in range(i+1,n):
                #We will subtract the components of i from each polynomial proceeding
                #Morally: p[j] = p[j] - innerProd(p[i], p[j])*p[i])
                ip_PiPi = self.innerProd(P[i], P[i])
                ip_PiPj = self.innerProd(P[i], P[j])

                P[j] = np.polysub(P[j], ip_PiPj/ip_PiPi*P[i])

        #Renormalize
        for i in range(n):
            normi = self.norm(P[i])
            if(normi != 0.0):
                P[i] = (1.0/normi)*P[i]
            else:
                P[i] = 0.0*P[i]


    def print(self):
        for i in range(len(self.points)):
            print(self.points[i].real, self.points[i].imag, self.weights[i])
        print()

    def toString(self):
        """
        Like print, but returns a string so we can direct output manually.
        """
        outStr = ""

        for i in range(len(self.points)):
            outStr += " ".join([str(s) for s in [self.points[i].real, self.points[i].imag, self.weights[i], "\n"]])
            if(i in self.point_jumps):
                outStr += "\n"
        outStr += "\n"

        return outStr

    def polyToString(self, P):
        """
        Outputs P evaluated at all points of self as a string.

        Since complex, prints both abs and phase. (But phase graphs are weird lol)
        """
        outStr = ""
        evaluateA = abs(polymod.eval(P, self.points))
        evaluateP = [cm.phase(z) for z in polymod.eval(P, self.points)]

        for i in range(len(self.points)):
            outStr += " ".join([str(s) for s in [self.points[i].real, self.points[i].imag, evaluateA[i], evaluateP[i], "\n"]])
            if(i in self.point_jumps):
                outStr += "\n"
        outStr += "\n\n"

        return outStr

    def evalToString(self, E):
        """
        The function E must have a multipoint evaluation method, E.eval(points)
        """
        outStr = ""
        evaluate = E.eval(self.points)
        for point, value in zip(self.points, evaluate):
            outStr += " ".join([str(s) for s in [point.real, point.imag, value, "\n"]])
        outStr += "\n"
        return outStr

def default():
    """
    Incase you just need SOME single variable complex weighted measure.
    """

    point_jumps = []

    points  = [0.0, 0.0+1.0j, 0.0-1.0j, 1.0, -1.0]

    #Even weights
    weights = [1.0/len(points) for i in points]

    return weighted_measure_complex_1(weights, points)


if __name__ == "__main__":

    mu = weighted_measure_complex_1([0.3, 0.3, 0.3], [0+1j, 1, 2])
    nu = weighted_measure_complex_1.default()

    print("mu")
    print(str(mu))

    print("nu")
    print(nu)

    print("Sum")
    print(mu+nu)

    print("mu/1.5")
    print(mu/1.5)

