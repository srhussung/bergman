import sys
from chebyshev import *

def bergman(numPoints, maxDegree, printPolys = False, printBergman = True, normalize = True):
    """
    This function will use the Chebyshev generator from the fastLeja project to construct Bergman Functions of several degrees.
    In the end, we would like to look at Bergman functions generated from weighted weakly admissable meshes, as well as from 
    different original bases. (Eventually it would be nice to have Gram-Schmidt with weighting and variable bases.) Maybe this 
    would be a good matlab task? Who knows. I am much more comfortable in Python, but matlab seems to be the standard.
    """

    X = [-1.0 + 2.0*i/(numPoints-1) for i in range(numPoints)]
    A = [Chebyshev(maxDegree, x, arrayOut = True) for x in X]

    #If you want to do this in higher dimensions, you will need to make this dependent.
    Nk = maxDegree

    #So A[0][3] is the 0th point, 3rd degree polynomial (Maybe we want to transpose this. Hmm.)
    #A must be referenced: point, degree

    #Test: Printout all polynomials, with line breaks
    if(printPolys):
        for degree in range(maxDegree):
            for point in range(numPoints):
                print(X[point], A[point][degree])
            print()

    bergman = [0.0 for point in range(numPoints)]
    if(printBergman):
        for point in range(numPoints):
            for degree in range(maxDegree):
                bergman[point] += A[point][degree]**2
            bergman[point] /= Nk

        for point in range(numPoints):
            print(X[point], bergman[point])


if __name__ == "__main__":
    #Default run options
    printPolys   = True
    printBergman = False

    #Parameters
    numPoints = 200
    maxDegree = 10

    args = sys.argv[1:]
    while(len(args) > 0):
        if  (len(args) > 1 and (args[0] == "-pp") or (args[0] == "--printPolys")):
            printPolys = args[1] in ["True", "true"]
            args = args[2:]
        elif(len(args) > 1 and (args[0] == "-pb") or (args[0] == "--printBerg")):
            printBergman = args[1] in ["True", "true"]
            args = args[2:]
        elif(len(args) > 1 and (args[0] == "-np") or (args[0] == "--numPoints")):
            numPoints = int(args[1])
            args = args[2:]
        elif(len(args) > 1 and (args[0] == "-md") or (args[0] == "--maxDegree")):
            maxDegree = int(args[1])
            args = args[2:]
        else:
            print("You made a bad argument! Owned by logic. (Check the arguments).")
            print("Remaining arguments: ", args)
            exit()

    bergman(numPoints, maxDegree, printPolys = printPolys, printBergman = printBergman)

