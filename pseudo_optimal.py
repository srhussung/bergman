#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.linalg import solve
from scipy.optimize import minimize
from scipy.optimize import LinearConstraint
from math import log
from sys import argv
from itertools import product

#Mine
import weighted_measure_complex_1 as wmc1

#Args will be done later

"""
Gameplan:

Create Points and Weights -> Weighted Measure

Craft matrix P of log(|z_i - z_j|)

Create vector q of log(|z_0 - z_k|)

Create "Simplex Derivative" matrix D with dimension n-1 x n

Form final system:
    A = 2[  DP  ]
         [1....1]
    b = Dq

Solve Aw = b
"""

def PseudoPotential(n, z_0):
    """
    Solves for the discrete weighted energy minimizing distribution
    given n equally spaced points in the unit interval and a charge 
    of opposite polarity at z_0, which is outside the unit interval.
    """

    #Create Points and Weights -> Weighted Measure
    points  = [-1.0 + (2.0*i)/(n-1) for i in range(n)]
    weights = [1/n for _ in range(n)]

    #Craft matrix P of log(|z_i - z_j|)
    P = np.matrix([[0.0 if z == w else log(abs(z - w)) for z in points] for w in points])

    #Create vector q of log(|z_0 - z_k|)
    q = np.matrix([[log(abs(z - z_0))] for z in points])

    #Create "Simplex Derivative" matrix D with dimension n-1 x n
    D = np.matrix(np.zeros((n-1, n)))

    for i in range(n-1):
        D[i,i] = 1
        D[i,i+1] = -1


    # - - - - - - - - - - - - - - - - - - #
    # - - - THIS BLOCK FOR DELETION - - - #
    # - - - - - - - - - - - - - - - - - - #

    ##Option 1: Constraints are not necessarily exact
    ##Form final system:
    ##    A = 2[  DP  ]
    ##         [1....1]
    ##
    ##    b = [ Dq]
    ##        [ 1 ]
    #A = np.matrix(np.zeros((n,n)))
    #A[0:n-1] = 2*D*P

    #for i in range(n):
    #    A[n-1,i] = 1.0


    #b = np.matrix(np.zeros((n,1)))
    #b[0:n-1] = D*q
    #b[n-1,0] = 1.0

    ##Solve the system within constraints
    ###Solving for whatever w, including negative weights -- weird!
    ##w = solve(A,b)
    #fun = lambda w : np.linalg.norm(A*vec(w) - b)

    #result = minimize(fun, weights, bounds = bounds)
    #weights = result.x

    # - - - - - - - - - - - - - - - - - - -#
    # - - - ABOVE BLOCK FOR DELETION - - - #
    # - - - - - - - - - - - - - - - - - - -#

    #Option 2: Native constraints

    #Our system is
    #    A = 2[  DP  ]
    #    b = [ Dq]

    #Constraints and bounds are handled by the solver.

    def constraint(weights):
        return sum(weights)-1.0
    constraints = {'type':'eq', 'fun' : constraint}

    #Element-wise bounds
    bounds = [(0.0, 1.0) for __ in weights]

    vec = lambda v : np.matrix([[x] for x in v])

    #Set up system and solve
    A = 2*D*P
    b = D*q
    fun = lambda w : np.linalg.norm(A*vec(w) - b)
    result = minimize(fun, weights, bounds = bounds, constraints = constraints)
    weights = result.x

    #Return
    mu = wmc1.weighted_measure_complex_1(weights, points)
    return mu


#Try several runs:
nlist = [50, 100]
zlist = [2, 1.4, 1.2, 1.1]
for n, z_0 in product(nlist, zlist):
    mu = PseudoPotential(n, z_0)
    plt.title("z_0 = " + str(z_0) + ", n = " + str(n))
    plt.plot(mu.points, mu.weights)
    plt.show()
