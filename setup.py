#System
import subprocess as sub

#Collections
from itertools import product

#Math
import math as math
import cmath as cm
import numpy as np

#Weighted Measure clases
import weighted_measure_parent as wm
import weighted_measure_real_1 as wmr1
import weighted_measure_real_2 as wmr2
import weighted_measure_complex_1 as wmc1

def processArgs(args):
    """
    Process arguments for the main.py function. This is to offload some of the really heavy administrative lifting in the main code file.
    """

    #Set default values
    n = 3000

    dim = 1

    #Stop conditions: iterations or g-efficiency
    stopCondition = "g-efficiency"
    #stopCondition = "iterations"

    gEfficiencyReq = 0.98
    outFileNum = 5

    BergmanDegree = 12
    displayPoints = 1000
    outFolder = "./output"
    writeToOutput = True

    #Placing and Weighting
    Placing = "Even"
    #Placing = "ComplexDisk"
    #Placing = "ComplexRectangle"

    Weighting = "Even"
    #Weighting = "PointMass"
    #Weighting = "FarPointMass"

    #For disk placing
    radii = 20

    #Whether or not to use Gram-Schmidt or matrix Bergman
    matrixBergman = False

    #Process args
    while(len(args) > 1):
        #print(args)
        if(args[0] in ["-n","--numPoints"]):
            n = int(args[1])
        elif(args[0] in ["-d", "--dim","--dimension"]):
            dim = int(args[1])
        elif(args[0] in ["-s", "-sc", "--stop-condition"]):
            stopCondition = args[1]
        elif(args[0] in ["-fn", "-nf", "--file-num", "--out-file-num"]):
            outFileNum = int(args[1])
        elif(args[0] in ["-g", "-ge", "--g-efficiency"]):
            gEfficiencyReq = float(args[1])
        elif(args[0] in ["-bd", "--bergman-degree"]):
            BergmanDegree = int(args[1])
        elif(args[0] in ["-dp","--display-points"]):
            displayPoints = int(args[1])
        elif(args[0] in ["-o","--output-folder"]):
            outFolder = "./" + args[1]
        elif(args[0] in ["-wo","--write-output"]):
            writeToOutput = (args[1] in ["True", "true", "T", "t"])
        elif(args[0] in ["-p", "--placing"]):
            Placing = args[1]
            if(Placing not in ["Even", "Cheby", "ComplexDisk", "ComplexRectangle"]):
                print("Not a valid placing scheme, quitting")
                exit()
        elif(args[0] in ["-w", "--weighting"]):
            Weighting = args[1]
            Options = ["Even", "Anti"]
            if(Weighting not in Options):
                print("Incorrect Weighting:", Weighting, "is not an option. Quitting")
                print("Valid options are: ", *Options)
                exit()
        elif(args[0] in ["-m", "--matrix-bergman"]):
            matrixBergman = (args[1] in ["True", "true", "T", "t"])
        elif(args[0] in ["-r", "--radii"]):
            radii = int(args[1])
        else:
            print("Command line error. Unrecognized command: ", args[1])
            exit()
        args = args[2:]

    #Check for any remaining
    if(len(args) == 1):
        print("Hanging argument: ", args[0])
        print("Continuing.")

    #Output parameters to metadata file
    #Clean folder if exists, make fresh if not. (This will not delete slurm files!)
    if(0 == sub.call(["test", "-e", outFolder])):
        sub.run(" ".join(["rm", "-f", outFolder+"/*dat"]), shell=True)
    else:
        sub.run(["mkdir", outFolder])

    #TODO Output meta data
    metaOutFile = open("/".join([outFolder,"meta.dat"]), "w")

    #For each arg, print human name, option, value in parseable way
    metaOutFile.write("\t".join(["Human Name      ", "-Option", "Value"            , "\n"]))  #This is the header
    metaOutFile.write("\t".join(["Number of Points", "-n     ", str(n)             , "\n"])) 
    metaOutFile.write("\t".join(["Dimension       ", "-d     ", str(dim)           , "\n"])) 

    metaOutFile.write("\t".join(["Stop Condition  ", "-s     ", str(stopCondition) , "\n"]))
    if(stopCondition == "iterations"):
        metaOutFile.write("\t".join(["Number of Files ", "-fn    ", str(outFileNum)    , "\n"]))
    elif(stopCondition == "g-efficiency"):
        metaOutFile.write("\t".join(["G-Efficiency    ", "-g     ", str(gEfficiencyReq)   , "\n"]))

    metaOutFile.write("\t".join(["MBergman Degree ", "-bd    ", str(BergmanDegree) , "\n"])) #Note: M means mathematical: this is to disambiguate between meta.dat folders from before clarifying and after. 
    metaOutFile.write("\t".join(["Display Points #", "-dp    ", str(displayPoints) , "\n"])) #      Other folders meta.dat files will not contain M, and this means that the degree is the dimension of P(X).
    metaOutFile.write("\t".join(["Output Folder   ", "-o     ", str(outFolder)     , "\n"]))
    metaOutFile.write("\t".join(["Write to Output ", "-wo    ", str(writeToOutput) , "\n"]))
    metaOutFile.write("\t".join(["Placing         ", "-p     ", Placing            , "\n"]))
    metaOutFile.write("\t".join(["Radii           ", "-r     ", str(radii)         , "\n"]))
    metaOutFile.write("\t".join(["Weighting       ", "-w     ", Weighting          , "\n"]))
    metaOutFile.write("\t".join(["Matrix Bergman  ", "-m     ", str(matrixBergman) , "\n"]))

    metaOutFile.close()

    #Return parameters
    return (n, dim, stopCondition, outFileNum, gEfficiencyReq, BergmanDegree, displayPoints, outFolder, writeToOutput, Placing, Weighting, matrixBergman, radii)
from math import *

def buildWeightedMeasure(dim, n, Placing, Weighting, radii, cutoff):

    points = []
    isComplex = False

    #Based on Placement option, choose appropriate class
    wmClass = "None"

    if(dim == 1):
        #Construct original weighted measure
        if(Placing == "Even"):
            #Original
            points  = [-1.0 + 2.0/(n-1)*i for i in range(n)]
            wmClass = wmr1.weighted_measure_real_1
        elif(Placing == "Cheby"):
            #Extra Crispy (Chebyshev)
            a = -1.0
            b =  1.0
            points = [0.5*(a+b) + 0.5*(b-a)*cos((2*i)/(2*(n-1))*pi) for i in range(n)]
            points.sort()
            wmClass = wmr1.weighted_measure_real_1
        elif(Placing == "ComplexDisk"):
            isComplex = True
            #Note that we want n points all together. How many subdisks should there be?
            #This is a nontrivial question. 

            #Activate to check the builder
            printCheck = False
            perradii = n//radii
            if(printCheck):
                print()
                #print("Making", radii, "circles with average", perradii, "points each")
            points = [0.0+0.0j]
            #for i in range(1,radii):
            for r in [(i/(radii-1))**0.2 for i in range(1,radii)]:
                #r = i/(radii-1)
                #r = (i/(radii-1))**0.5
                #r = (i/(radii-1))**0.2
                z = cm.exp(0.0+2*pi/(perradii-1)*1.0j)
                for k in range(perradii):
                    points += [r*(z**k)]
                    if(printCheck):
                        print(points[-1].real, points[-1].imag)
                if(printCheck):
                    print()

            if(printCheck):
                exit()
            wmClass = wmc1.weighted_measure_complex_1
        elif(Placing == "ComplexRectangle"):
            isComplex = True
            #TODO Make this more even somehow
            nsqrt = int(sqrt(n))

            #Real part
            a = -10
            b =  10
            X = [a + (b-a)*i/(nsqrt-1) for i in range(nsqrt)]
            X = np.array(X)

            #Complex part
            c = -1
            d = 1
            Y = [c + (d-c)*i/(nsqrt-1) for i in range(nsqrt)]
            Y = np.array(Y)*1.0j

            points = np.sum(list(product(X,Y)), 1)
            wmClass = wmc1.weighted_measure_complex_1
        else:
            print("No placing scheme specified, quitting")
            exit()
    elif(dim == 2):
        if(Placing == "Even"): #Assume Square
            sqrtn = math.sqrt(n)
            n = int(sqrtn)
            m = int(sqrtn)
            #n = int(math.sqrt(2/3)*sqrtn)
            #m = int(math.sqrt(1/3)*sqrtn)
            X = [-1 + (2.0)*i/(n-1) for i in range(n)]
            Y = [-1 + (2.0)*i/(m-1) for i in range(n)]

            points = list(product(X,Y))
            wmClass = wmr2.weighted_measure_real_2
        elif(Placing == "EvenDisk"): #Real disk, dimension 2
            print("Haven't implemented real disk dim 2, exiting")
            exit()
        else:
            print("No other dimension two placings.")
            exit()
    elif(dim == 3):
        print("todo, dim 3")
    else:
        print("Can only do dimensions 1, 2, 3 right now: apologies. Quitting")
        exit()

    #Weight Points
    n = len(points)

    #Construct basic measures

    # - - - Even - - - #
    weights = [1.0/len(points) for _ in points]
    mu_even = wmClass(weights, points, cutoff = cutoff)

    # - - - Point mass - - - #
    mu_point = wmClass([1.0], [1.0], cutoff = cutoff)
    mu_far_point = wmClass([1.0], [3.0], cutoff = cutoff)

    if(not isComplex and dim == 1):
        # - - - Anti weighting - - - #
        #This is from experimental evidence. The worst interval distribution
        #is just half (almmost) the mass at two points near 0.0

        #Find index of point closest to +-0.064: Distribute mass mostly at nearby points
        toDistribute = 0.9
        spread = 3 #Careful changing

        #Add a tiny amount to everywhere else.
        remainder = (1.0 - toDistribute)/len(points)
        weights = [remainder for _ in range(len(points))]

        indexA = 0
        while(points[indexA] < -0.064):
            indexA += 1

        indexB = len(points) - 1
        while(points[indexB] > +0.064):
            indexB -= 1

        #Adjust    0.064 iA -> iA 0.064
        indexA -= 2
        for i in range(3):
            weights[indexA + i] = toDistribute/6.0
            weights[indexB + i] = toDistribute/6.0
        mu_anti = wmr1.weighted_measure_real_1(weights, points, cutoff = cutoff)

    if(Weighting == "Even"):
        mu = mu_even
    elif(Weighting == "PointMass"):
        a = 0.5
        mu = (1.0-a)*mu_even + a*mu_point
    elif(Weighting == "FarPointMass"):
        a = 0.2
        mu = (1.0-a)*mu_even + a*mu_far_point
    elif(dim == 1):
        if(Weighting == "Anti"):
            mu = mu_anti
    elif(dim == 2):
        print("No other weights for dim = 2 specified, check setup.py")
    elif(dim == 3):
        print("No other weights for dim = 3 specified, check setup.py")
    else:
        print("No weighting scheme specified, quitting")
        exit()

    return mu
