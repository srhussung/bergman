import numpy as np
import numpy.random as npr
from math import sqrt
import matplotlib.pyplot as plt

"""
We are generating points to test the ability of the Titterington-Torsney algorithm to give a polynomial representation of set boundary. We generate the points here.
"""

def make_points_approx(num_points, lb, sharpness):
    """
    Internal method, doesn't get consistent numbers of points
    """
    #Get random points and adjust position
    plane_points = npr.rand(num_points,2)
    plane_points = 2*plane_points - np.array([[1, 1]])

    #Define star shaped function
    def f(theta):
        return lb + (1.0 - lb)*np.cos(2*theta)**sharpness

    polar = np.array([[np.sqrt(x**2 + y**2), np.arctan2(y, x)] for x,y in plane_points])
    mask = f(polar[:, 1]) > polar[:, 0]
    plane_points = plane_points[mask]

    #Result Plot
    return [list(p) for p in plane_points]

def make_points(num_points, lb = 0.2, sharpness = 6):
    """
    Call with the parameters you want
    """
    points = []
    factor = 1
    while(len(points) < num_points):
        points.extend(make_points_approx((num_points-len(points))*factor, lb, sharpness))

        #Since we want num_points - len(points) more points, we adjust by what we got last time.
        #Usually gets the answer in 2 guesses
        factor = round(max(1, num_points/len(points)))
        
    points = points[:num_points]
    return np.array(points)

if __name__ == "__main__":
    num_points = 300
    points = make_points(num_points)

    #print(points)
    #print(len(points))

    plt.plot(points[:, 0], points[:, 1], marker=".", linestyle="none")
    plt.show()



