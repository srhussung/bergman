#Imports
import numpy as np
import numpy.polynomial.polynomial as npp

#Mine
import polynomial as polymod
import weighted_measure_parent as wm
import weighted_measure_real_2 as wmr2

import bergman

class bergman_kernel_real_2(bergman.bergman_kernel):
    """
    This is a Bergman class file that will hold information needed to evaluate the Bergman Kernel. 

    This in particular is the REAL TWO-DIMENSIONAL bergman class file

    This will be the polynomial basis, (after gram-schmidt? before?)

    The Gram matrix relative to this basis, and perhaps an inverse (although... ew. How stable is this?)

    The Bergman Kernel initializer may construct an honest polynomial, which will be stored and used for computation.

    The idea is that this will have an evaluate method accepting a numpy array of points, and it will return an array with this bergman kernel evaluated at each of these points

    use np.asarray, paired with if(A.shape == ()) to detect scalars.
    """

    #Big settings (should not change within run)
    isComplex = False

    originalBasis = []
    orthonormalBasis = []
    bergmanPoly = []
    mu = []
    dim = 2
    degree = 0
    oldEvals = {}

    
    def __init__(self, mu, n, inbasis = []):
        """
        Currently a copy of weighted measure code

        This will generate the BergmanKernel of degree n. 

        "Note: Cannot return with |p| intact, so the complex case 
        would require some real work. Not clear how to do this."

        mu - a weighted measure, carries points and weights
        
        n  - dimension of polynomial space
        """

        self.degree = n

        if(inbasis==[]):
            #Generate basis. [[0 0 1], [0 1 0], [1 0 0]]
            B = polymod.monoBasis(n, self.dim)
            #print("Monobasis:")
            #print(B)
        else:
            #TODO Add checks for n matching the basis, square basis, etc
            B = inbasis

        self.originalBasis = B

        #Orthonormalize
        mu.GramSchmidt(B, dim=2)

        self.orthonormalBasis = B
        print("O-N Basis:")
        print(B)

        #Morally: The Bergman Kernel is the sum of the squares of the norms 
        #of each p in the basis: sum |p|^2

        #Square
        Squares = [0.0 for i in range(n)]
        for i in range(n):
            #Note custom method
            Squares[i] = polymod.polymul2d(B[i], B[i])

        #Sum
        polytotal = np.zeros([1, 1])
        for poly in Squares:
            polytotal = polymod.polyadd2d(polytotal, poly)

        #Normalize
        polytotal /= n

        #Store self variables
        self.isComplex = mu.isComplex
        self.mu = mu
        self.oldEvals = {}

        #Return
        #return polytotal
        self.bergmanPoly = polytotal

    def eval(self, points):
        """
        Since this is a real, two-variable polynomial, we can evaluate at all points
        just using the numpy evaluation code.
        """
        pointArray = np.asarray(points)

        #Unpack xpoints if needed
        if len(pointArray.shape) == 1: #Single point
            xpoints1, xpoints2 = pointArray
        else: #List of points
            xpoints1, xpoints2 = list(zip(*pointArray))

        ypoints = npp.polyval2d(xpoints1, xpoints2, self.bergmanPoly)

        return ypoints

def default(mu = wmr2.default()):
    """
    If you just need SOME Bergman function
    """
    n = 4
    B = bergman_kernel_real_2(mu, n)

    return B
    

if __name__ == "__main__":
    mu = wmr2.default()
    B = default(mu)
    ypoints = B(mu.points)

    #for i in range(len(mu.points)):
    #    print(mu.points[i][0], mu.points[i][1], ypoints[i])

    for point, yPoint in zip(mu.points, B(mu.points)):
        print(point[0], point[1], yPoint)

    #print()
    #print("Testing individual points")
    #x = [0.4, 0.4]
    #a = B.eval(x)
    #print(x, B.eval(x))
    #print(x, B(x))

    print("Bergman Poly")
    print(B.bergmanPoly)


