import cmath as cm
import numpy as np
import weighted_measure_parent as wm

class bergman_kernel:
    """
    This is a Bergman class file that will hold information needed to evaluate the Bergman Kernel, specifically, the normalized Bergman kernel.

    This will be the polynomial basis, (after gram-schmidt? before?)

    The Gram matrix relative to this basis, and perhaps an inverse (although... ew. How stable is this?)

    Depending on whether or not the domain is complex, the Bergman Kernel initializer may construct an honest polynomial, which will be stored and used for computation.
    Otherwise, when points need to be evaluated, this will have to be done via the x^T G^{-1} x, where x is the vector of polynomials, each evaluated at this point.
    (Can the whole computation be done through mass computation and matmuls? I don't think so. One to many dimensions maybe, but I can't grok it. 
        This is a lot of computation though. Eesh.


    The idea is that this will have an evaluate method accepting a numpy array of points, and it will return an array with this bergman kernel evaluated at each of these points

    perhaps np.asarray, paired with if(A.shape == ()) to detect scalars.

    """

    isComplex = ""
    originalBasis = []

    def __init__(self, mu, n, dim, inbasis = []):
        pass

    def eval(self, points):
        """
        Should not be called externally, meant to be used for overloading: see def __call__ below
        """
        pass

    def __call__(self, points):
        return self.eval(points)

