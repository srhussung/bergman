"""
The purpose of this program is to find a measure which minimizes the value of B_mu(z_0). To do this, we utilize a two step algorithm. 

1. We solve the discrete weighted potential theory problem, varying the weights w_i of the points z_i in order to minimizize the potential theoretic energy of the measure (z_i, w_i) and (z_0, 1), ignoring self terms.

2. Now that we have obtained a (hopefully) near solution, we can use the KKT conditions and STT algorithm to find the true minimimizing measure mu.
"""


