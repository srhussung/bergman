#!/usr/bin/env python3

import numpy as np
from math import *
from sys import argv
import subprocess
import copy

#Mine
import polynomial as polymod
import setup

#Bergman classes
import bergman
import bergman_real_1 as br1
import bergman_real_2 as br2
import bergman_complex_1 as bc1

#Weighted Measure clases
import weighted_measure_parent 
import weighted_measure_real_1 as wmr1
import weighted_measure_real_2 as wmr2
import weighted_measure_complex_1 as wmc1

"""
Command line specified parameters are 

n -p Number of Points to use in the weighted measure

OutfileNum -fn

BergmanDegree -d Degree Polynomials to consider in constructing Bergman Kernel

DisplayPoints -dp Number for displaying

outputFolder -o Folder for output

writeToOutput -wo Whether or not to write to output: True/true/T/t or other

Placing -p Even, Cheby

Weighting -w Even, Arcsin

MatrixBergman -m True/False/T/t/F/f
"""

#Non command line parameters
massTol = 1.0e-02
polTol = 1.0e-02

#This cutoff is how small weights must be before they are removed. 
#You get accurate results with this set pretty high--just keep it enough under the inital point weight!
#cutoff = 1e-04
cutoff = 0

printMatrix = False
writeFinal = True

#Strip args
args = argv[1:]
#Process command line args

(n, dim, stopCondition, outFileNum, gEfficiencyReq, BergmanDegree, displayPoints, outFolder, writeToOutput, Placing, Weighting, matrixBergman, radii) = setup.processArgs(args)

#Construct original weighted measure
mu = setup.buildWeightedMeasure(dim, n, Placing, Weighting, radii, cutoff)

#Normalize if mu not already of mass 1
mass = mu.mass()
if(abs(1.0 - mass) > massTol):
    mu /= mass

#Store a copy of mu
mu_original = copy.deepcopy(mu)

if(writeToOutput or writeFinal):
    #Setup output files
    measureOutFile = open("/".join([outFolder,"output.dat" ]), "w")
    bergmanOutFile = open("/".join([outFolder,"bergman.dat"]), "w")
    minMaxOutFile  = open("/".join([outFolder,"minMax.dat"]), "w")

#Main loop.
i = 0
gEff = 0.0
gramDeterminant = 0.0
minMax = [0.0, 0.0]

while((stopCondition == "iterations" and i < outFileNum) or \
      (stopCondition == "g-efficiency" and gEff < gEfficiencyReq)):

    #Compute
    if(matrixBergman):
        #Output TiTor Function based on P^T M^{-1} P (Prenormalized)
        print("Try again, matrix bergman depreciated.")
        B = mu.TiTorFunction(BergmanDegree+1, dim)
    else:
        ##Output Bergman Kernel from Gram-Schmidt procedure and Normalize
        #B = mu.BergmanKernel(BergmanDegree+1, dim)/(BergmanDegree+1)

        #Construct Bergman Kernel
        if(dim == 1 and not mu.isComplex):
            bergman = br1.bergman_kernel_real_1(mu, BergmanDegree+1)
        elif(dim == 2 and not mu.isComplex):
            bergman = br2.bergman_kernel_real_2(mu, BergmanDegree+1)
        elif(dim == 1 and mu.isComplex):
            bergman = bc1.bergman_kernel_complex_1(mu, BergmanDegree+1)
        else:
            print("Don't have this bergman built yet: main.py")


    #Get min and max
    evaluated = bergman(mu_original.points)
    minMax[0] = np.min(evaluated)
    minMax[1] = np.max(evaluated)
    newEff = 1.0/minMax[1]
    if gEff > newEff:
        print("Hey! gEfficiency is decreasing from", gEff, "to", newEff)

    gEff = 1.0/minMax[1]

    #Get Gram Determinant
    newGramDeterminant = np.linalg.det(mu.Gram(BergmanDegree+1))

    if gramDeterminant > newGramDeterminant:
        print("Hey! gramDeterminant is decreasing from")
        print(gramDeterminant)
        print("to")
        print(newGramDeterminant)

    gramDeterminant = newGramDeterminant

    #Print it!
    if printMatrix:
        for row in mu.Gram(BergmanDegree+1):
            for number in row:
                print(abs(number), end="")
            print()

    #Output Measure and Bergman Kernel
    if(writeToOutput and dim == 1):
        #Output Measure
        measureOutFile.write("".join(["#mu-", str(i),"\n"]))
        measureOutFile.write(mu.toString())
        measureOutFile.write("\n")

        #Output Bergman
        bergmanOutFile.write("".join(["#Normalized-Bergman-Kernel-", str(i),"\n"]))
        bergmanOutFile.write(mu_original.evalToString(bergman))
        bergmanOutFile.write("\n")

        #Output biggest and smallest of Bergman.
        minMaxOutFile.write(" ".join([str(s) for s in [i] + minMax + ["\n"]]))
    elif(dim == 2 or dim == 3):
        print("Don't know how to output dim 2 or 3 yet.")
    

    #Update Weighted Measure
    mu = bergman*mu

    print("iteration %3d mass %8.7f max %10.7f Gram-Det %6.4f G-eff %6.4f points %4d" % \
            (i, mu.mass(), minMax[1], gramDeterminant**(1.0/(BergmanDegree+1)), 100*gEff, len(mu.points)))

    i += 1
    

if(writeToOutput or writeFinal):
    #One last printout of both measure and Bergman Kernel.
    i = outFileNum 
    measureOutFile.write("".join(["#mu-", str(i),"\n"]))
    measureOutFile.write(mu.toString())
    #Compute
    if(matrixBergman):
        #Output TiTor Function based on P^T M^{-1} P (Prenormalized)
        print("Try again, matrix bergman depreciated.")
        B = mu.TiTorFunction(BergmanDegree+1, dim)
    else:
        ##Output Bergman Kernel from Gram-Schmidt procedure and Normalize
        #B = mu.BergmanKernel(BergmanDegree+1, dim)/(BergmanDegree+1)

        #Construct Bergman Kernel
        if(dim == 1 and not mu.isComplex):
            bergman = br1.bergman_kernel_real_1(mu, BergmanDegree+1)
        elif(dim == 1 and mu.isComplex):
            bergman = bc1.bergman_kernel_complex_1(mu, BergmanDegree+1)
        else:
            print("Don't have this bergman built yet: main.py")

    bergmanOutFile.write("".join(["#Normalized-Bergman-Kernel-", str(i),"\n"]))
    bergmanOutFile.write(mu_original.evalToString(bergman))
    evaluated = bergman(mu_original.points)
    minMax[0] = np.min(evaluated)
    minMax[1] = np.max(evaluated)
    minMaxOutFile.write(" ".join([str(s) for s in [i] + minMax + ["\n"]]))

    #Close up shop!
    measureOutFile.close()
    bergmanOutFile.close()
    minMaxOutFile.close()
