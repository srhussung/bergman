import numpy as np
import numpy.polynomial.polynomial as npp
from itertools import product

def eval(p, points):
    """
    This is a general evaluation function. The point is that it can 
    detect and then evaluate polynomials based on their dimension.
    However, this may slow down evaluation. We'll see. However, it is
    possible to hand this function an array in x, and evaluate all 
    values together.

    We assume points is an array where points[i] contains an array 
    {x_i, y_i, z_i} etc.
    """

    #Determine dimension via p
    dim = len(p.shape)

    if(dim < 1 or dim > 3):
        print("Your dimension is ", dim, "but we can only do 1, 2, 3")
        print("Exiting")
        exit()
    
    #Splice x if needed
    if(dim == 1):
        x = points
    elif(dim == 2):
        x, y = list(zip(*points))
    elif(dim == 3):
        x, y, z = list(zip(*points))

    #print("all good!")
    #print(x[0:10])
    #if(dim >= 2):
    #    print(y[0:10])

    #if(dim >= 3):
    #    print(z[0:10])

    """
    if(dim == 1):
        x = points[0]
    elif(dim == 2):
        x = points[0]
        y = points[1]
    elif(dim == 3):
        x = points[0]
        y = points[1]
        z = points[2]
    """

    #Double check for compatibility
    #umm... think through this later.

    #Call appropriate numpy function
    returnable = 0.0
    if(dim == 1):#TODO should change all to be this way, but the np.polyval takes coefs in the other order.
        p = np.flip(p)
        returnable = npp.polyval(x, p)
        p = np.flip(p)
    elif(dim == 2):
        returnable = npp.polyval2d(x, y, p)
    elif(dim == 3):
        returnable = npp.polyval3d(x, y, z, p)
    else:
        print("Beats me. Issue in polynomial.py eval")

    return returnable
    
def monoBasis(n, dim):
    """
    Returns an array of polynomials of size n defining the 
    monomial basis, highest to lowest degree.
    """
    if(dim == 1):
        B = [ np.array([(1.0 if (i == n-1-j) else 0.0)  for j in range(n)]) for i in range(n)]
    elif(dim == 2):
        B = [ np.array([[1 if (i == k//n and j == k%n) else 0 for j in range(n)] for i in range(n)]) for k in range(n*n)]

    return B

#def extend(B):
"""
This should add 0.0's where necessary to a "ragged" basis.
"""

def polymul2d(A, B):
    """
    Multiply two 2D polynomials together. Is it fast? Probably not. 
    
    This does not trim, so if there are trailing zeroes they will not be removed. However, 
    as long as the input polynomials are trimmed, this should not be an issue.
    """

    Ar, Ac = A.shape
    Br, Bc = B.shape
    C = [[0.0 for _ in range(Ac + Bc - 1)] for __ in range(Ar + Br - 1)]
    C = np.array(C)

    for i, j, k, l in product(*[range(s) for s in [*A.shape, *B.shape]]):
        C[i+k][j+l] += A[i][j]*B[k][l]

    return C

def polyadd2d(A, B):
    """
    Add two 2D polynomials together. Is it fast? Probably not. 
    
    This does not trim, so if there are trailing zeroes they will not be removed. However, 
    as long as the input polynomials are trimmed, this should not be an issue.
    """

    Ar, Ac = A.shape
    Br, Bc = B.shape
    
    C = np.zeros([max(x) for x in zip(A.shape, B.shape)])

    for i, j in product(*[range(s) for s in A.shape]):
        C[i][j] += A[i][j]

    for i, j in product(*[range(s) for s in B.shape]):
        C[i][j] += B[i][j]

    return C

def polysub2d(A, B):
    return polyadd2d(A, (-1)*B)

def printPoly(p, a, b, n):
    """
    This will display the polynomial p between a and b
    using n points
    """
    points = [a + 1.0*(b-a)/(n-1)*i for i in range(n)]

    for point in points:
        print(point, np.polyval(p, point))

def toString(p, a, b, n):
    points = [a + 1.0*(b-a)/(n-1)*i for i in range(n)]
    outStr = ""
    #
    for point in points:
        outStr += " ".join([str(s) for s in [point, np.polyval(p, point), "\n"]])
    outStr += "\n"
    return outStr


if __name__ == "__main__":
    #    P = np.array([[1, 0, 4], [3, 1, 1], [0, 0, 8]])
    #    ax = -2.0
    #    bx = +2.0
    #    ay = -2.0
    #    by = +2.0
    #    n = 50
    #    m = 40
    #    X = [ax + (bx-ax)*i/(n-1)   for _ in range(n) for i in range(m)]
    #    Y = [ay + (by-ay)*i/(m-1)   for i in range(n) for _ in range(m)]
    #    #print(X)
    #    #print(Y)
    #    values = eval(P, [X, Y])
    #    for i in range(n):
    #        for j in range(m):
    #            print(X[i*m + j], Y[i*m+j], values[i*m+j])
    #        print()
    #
    #
    P = monoBasis(3, 2)
    print(P)
