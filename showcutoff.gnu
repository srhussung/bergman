#Short runs
set title "Standard plot, 500 Iterations"
plot "output_cutoff1e-3.dat" index 100 w l title columnheader(1), \
     "output_cutoff1e-4.dat" index 100 w l title columnheader(1), \
     "output_nocutoff.dat"   index 100 w l title columnheader(1)
pause -1

set title "Logscale plot, 100 Iterations"
set logscale y
replot
pause -1

#Long runs
set title "Standard plot, 500 Iterations"
unset logscale y
plot "output_long_cutoff1e-4.dat" index 500 w l title columnheader(1), \
     "output_long_nocutoff.dat"   index 500 w l title columnheader(1)
pause -1

set title "Logscale plot, 500 Iterations"
set logscale y
replot
pause -1

plot "output_long_nocutoff.dat"   index 500 w l title columnheader(1)
pause -1

