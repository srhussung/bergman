#Imports
import numpy as np

#Mine
import polynomial as polymod
import weighted_measure_parent as wm
import weighted_measure_real_1 as wmr1

import bergman

class bergman_kernel_real_1(bergman.bergman_kernel):
    """
    This is a Bergman class file that will hold information needed to evaluate the Bergman Kernel. 

    This in particular is the REAL SINGLE DIMENSIONAL bergman class file

    This will be the polynomial basis, (after gram-schmidt? before?)

    The Gram matrix relative to this basis, and perhaps an inverse (although... ew. How stable is this?)

    Depending on whether or not the domain is complex, the Bergman Kernel initializer may construct an honest polynomial, which will be stored and used for computation.
    Otherwise, when points need to be evaluated, this will have to be done via the x^T G^{-1} x, where x is the vector of polynomials, each evaluated at this point.
    (Can the whole computation be done through mass computation and matmuls? I don't think so. One to many dimensions maybe, but I can't grok it. 
        This is a lot of computation though. Eesh.

    The idea is that this will have an evaluate method accepting a numpy array of points, and it will return an array with this bergman kernel evaluated at each of these points

    use np.asarray, paired with if(A.shape == ()) to detect scalars.
    """

    #Big settings (should not change within run)
    isComplex = False
    memoizeEvals = False

    originalBasis = []
    orthonormalBasis = []
    bergmanPoly = []
    mu = []
    dim = 1
    degree = 0
    oldEvals = {}

    
    def __init__(self, mu, n, inbasis = []):
        """
        Currently a copy of weighted measure code

        This will generate the BergmanKernel of degree n. 

        "Note: Cannot return with |p| intact, so the complex case 
        would require some real work. Not clear how to do this."

        mu - a weighted measure, carries points and weights
        
        n  - dimension of polynomial space
        """

        self.degree = n

        if(inbasis==[]):
            #Generate basis. [[0 0 1], [0 1 0], [1 0 0]]
            B = polymod.monoBasis(n, self.dim)
        else:
            #TODO Add checks for n matching the basis, square basis, etc
            B = inbasis

        self.originalBasis = B

        #Orthonormalize
        mu.GramSchmidt(B)

        self.orthonormalBasis = B

        #Morally: The Bergman Kernel is the sum of the squares of the norms 
        #of each p in the basis: sum |p|^2

        #Square
        Squares = [0.0 for i in range(n)]
        for i in range(n):
            #Squares[i] = np.polymul(B[i], B[i].conjugate()) TODO I think this line should be used--but where did the complex structure go?
            Squares[i] = np.polymul(B[i], B[i])

        #Sum
        polytotal = [0.0]
        for poly in Squares:
            polytotal = np.polyadd(polytotal, poly)

        #Normalize
        polytotal /= n

        #Store self variables
        self.isComplex = mu.isComplex
        self.mu = mu
        self.oldEvals = {}


        #Return
        #return polytotal
        self.bergmanPoly = polytotal

    def eval(self, points):
        """
        Since this is a real, single variable polynomial, we can evaluate at all points
        just using the numpy evaluation code.
        """
        pointArray = np.asarray(points)

        if(self.memoizeEvals):
            pointArrayStr = str(pointArray.tolist())
            if(pointArrayStr not in self.oldEvals):
                ypoints = np.polyval(self.bergmanPoly, pointArray)
                self.oldEvals[pointArrayStr] = ypoints
            return self.oldEvals[pointArrayStr]
        else:
            ypoints = np.polyval(self.bergmanPoly, pointArray)
            return ypoints

def default(mu = wmr1.default()):
    """
    If you just need SOME Bergman function
    """
    n = 4

    B = bergman_kernel_real_1(mu, n)

    return B
    

if __name__ == "__main__":
    mu = wmr1.default()
    B = default(mu)
    ypoints = B.eval(mu.points)

    for i in range(len(mu.points)):
        print(mu.points[i], ypoints[i])

    print()
    print("Testing individual points")
    print("0.4", B.eval(0.4))
    print("0.4", B(0.4))


    
