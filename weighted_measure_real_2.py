#Debug
import pdb

#Numerics
import math
import numpy as np
from numpy import polyadd as padd
from numpy import polymul as pmul
from numpy import polyval as pval
import numpy.linalg as npl
import numpy.polynomial.polynomial as npp

#Iteration
from itertools import chain

#Custom
import polynomial as polymod
import weighted_measure_parent 

class weighted_measure_real_2(weighted_measure_parent.weighted_measure):
    """
    This will handle the REAL, 2-DIMENSIONAL weighted measure code.
    """


    def __add__(self, nu):
        """
        Overload + operator to add self and another weighted_measure nu in the real 2d case

        This had to be done since numpy can return some weirdness.
        """
        
        #Need to:
        #Take union of self.points, nu.points
        #Add weights when both have them, or just take the weight of one.
        #For each point, need to find this point in the other set (if it exists)
        new_points = []
        new_weights = []
        for point, weight in zip(self.points, self.weights):
            #If in self.points
            new_points.append(point)
            new_weights.append(weight)
            if point in nu.points: #If also in nu.points
                i = np.where((nu.points) == point)
                #pdb.set_trace()
                new_weights[-1] += float(nu.weights[i[0][0]])

        for point, weight in zip(nu.points, nu.weights):
            if point not in self.points: #If only in nu
                new_points.append(point)
                new_weights.append(weight)

        xi = type(self)(new_weights, new_points)

        return xi

    def innerProd(self, p, q):
        """
        Here p and q must be polynomial coefficient arrays, 
        evaluatable at all points in the weighted measure.

        Here we are evaluating \int_{\mu} p(z)q(z) d\mu. 
        """

        evaluated = polymod.eval(p, self.points)*          \
                    polymod.eval(q, self.points)*          \
                    self.weights

        return sum(evaluated)

    def GramSchmidt(self, P, dim = 1):
        """
        P must be an array of coefficient arrays.
        """
        n = len(P)
        #This method is a manual Gram Schmidt decomposition
        #Subtract projections
        for i in range(n):
            for j in range(i+1,n):
                #We will subtract the components of i from each polynomial proceeding
                #Morally: p[j] = p[j] - innerProd(p[i], p[j])*p[i])
                ip_PiPi = self.innerProd(P[i], P[i])
                ip_PiPj = self.innerProd(P[i], P[j])

                P[j] = polymod.polysub2d(P[j], ip_PiPj/ip_PiPi*P[i])

        #Renormalize
        for i in range(n):
            normi = self.norm(P[i])
            if(normi != 0.0):
                P[i] = (1.0/normi)*P[i]
            else:
                P[i] = 0.0*P[i]


    def print(self):
        for i in range(len(self.points)):
            print(self.points[i], self.weights[i])
        print()

    def toString(self):
        """
        Like print, but returns a string so we can direct output manually.
        """
        outStr = ""
        for i in range(len(self.points)):
            outStr += " ".join([str(s) for s in [" ".join([str(t) for t in self.points[i]]), self.weights[i], "\n"]])
            if(i in self.point_jumps):
                outStr += "\n"
        outStr += "\n"
        return outStr

    def evalToString(self, E):
        """
        The function E must have a multipoint evaluation method, E.eval(points)
        """
        outStr = ""
        evaluate = E.eval(self.points)
        for i in range(len(self.points)):
            outStr += " ".join([str(s) for s in [self.points[i], evaluate[i], "\n"]])
            if(i in self.point_jumps):
                outStr += "\n"
        outStr += "\n"
        return outStr

def default():
    """
    Incase you just need SOME 2 variable real weighted measure
    """

    n = 16

    m = int(math.sqrt(n))
    X = [-1 + (2.0)*i/(m-1)   for _ in range(m) for i in range(m)]
    Y = [-1 + (2.0)*i/(m-1)   for i in range(m) for _ in range(m)]
    points = list(zip(X,Y))

    #Even weights
    weights = [1.0/len(points) for i in points]

    return weighted_measure_real_2(weights, points)


if __name__ == "__main__":

    mu = weighted_measure_real_2([0.3, 0.3, 0.3], [[0, 0], [0, 1], [1, 0]])
    nu = weighted_measure_real_2([0.1, 0.1, 0.1], [[0, 0], [1, 1], [2, 3]])

    print("mu")
    print(str(mu))

    print("nu")
    print(nu)

    print("Sum")
    print(mu+nu)

    print("mu/1.5")
    print(mu/1.5)

